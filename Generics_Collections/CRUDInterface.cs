﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics_Collections
{
    public interface CRUDInterface<T>
    {
        void create(List<T> list);
        string read();
        T update();
        void delete(List<T> list);
    }
}
