﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics_Collections
{
    class Program
    {
     
        //metoda generica ce inverseaza un array
        public static T[] ReverseString<T>(T[] input)
        {
            T[] output = new T[input.Length];
            int contor = 0;
            foreach (T element in input.Reverse())
            {
                output[contor] = element;
                contor++;
            }

            return output;
        }

        static void Main(string[] args)
        {
            int[] array = {1,2,3,4,5,6,7,8,9,10};
            array = ReverseString(array);

            Console.WriteLine("1.Generic method that receives as input an array and returns it in reverse\n\n");

            foreach (int element in array)
            {
                Console.Write(element + " ");
            }
         
            Console.WriteLine("\n\n\n2.Create a class that has two generic parameters...Create a method inside the class that displays the types of both parameters.\n\n");
            MyClass<object,MyInterface> obj = new MyClass<object,MyInterface>();
            obj.Display();

            Console.WriteLine("\n\n3.Use a similar collection to list that doesn’t allow duplicate values.\n\n");
            HashSet<string> NoDupList = new HashSet<string>();
            NoDupList.Add("Ana");
            NoDupList.Add("Ana");
            NoDupList.Add("Ana");
            NoDupList.Add("are");
            NoDupList.Add("are");
            NoDupList.Add("mere");

            foreach (string elem in NoDupList)
            {
                Console.Write(elem + " ");
            }

            Console.WriteLine("\n\n\n4.Create a Customer and an Order class...\n");

            List<Order> orders = new List<Order>();
            orders.Add(new Order(100, "orderA"));
            orders.Add(new Order(200, "orderB"));
            orders.Add(new Order(300, "orderC"));
            orders.Add(new Order(400, "orderD"));
            orders.Add(new Order(500, "orderE"));

            List<Customer> customers = new List<Customer>();
            customers.Add(new Customer("Andrei", 22));
            customers.Add(new Customer("Tudor", 21));
            customers.Add(new Customer("Alexandru", 18));
            customers.Add(new Customer("Stefan", 30));
            customers.Add(new Customer("Mircea", 20));

            customers[0].AddOrder(orders[0]);
            customers[0].AddOrder(orders[1]);
            customers[1].AddOrder(orders[0]);
            customers[1].AddOrder(orders[1]);
            customers[1].AddOrder(orders[2]);
            customers[1].AddOrder(orders[3]);
            customers[2].AddOrder(orders[4]);
            customers[3].AddOrder(orders[1]);
            customers[3].AddOrder(orders[2]);
            customers[3].AddOrder(orders[3]);
            customers[4].AddOrder(orders[4]);
            customers[4].AddOrder(orders[1]);
            customers[4].AddOrder(orders[2]);

            Dictionary<string,List<Order>> dictionary = new Dictionary<string, List<Order>>();

            foreach (Customer customer in customers)
            {
                dictionary.Add(customer.Name, customer.orders);
            }

            foreach (KeyValuePair<string, List<Order>> customer in dictionary)
            {
                Console.Write("Customer: "+customer.Key + " Orders: ");
                foreach (Order order in customer.Value)
                {
                    Console.Write(order.ToString());
                }
                Console.WriteLine();

            }
            
            Console.WriteLine("\n\n5.For the previous created models create a generic interface that expose the CRUD.\n");
            //Construim noua lista customers2 si adaugam customers noi in aceasta folosind metoda create din interfata apoi folosim metoda read pentru afisarea informatiilor entitatilor
            //update pentru modificarea datelor acestora si delete pentru stergerea lor
            List<Customer> customers2 = new List<Customer>();

            Customer c1 = new Customer("Alex", 20);
            Customer c2 = new Customer("Popescu", 25);
            Customer c3 = new Customer("Vasilescu", 30);

            Order o1 = new Order(10000, "orderCar");
            Order o2 = new Order(550, "orderTV");
            Order o3 = new Order(50, "orderToy");

            c1.AddOrder(o1);
            c1.AddOrder(o2);
            c1.AddOrder(o3);
            c2.AddOrder(o3);
            c3.AddOrder(o2);
            c3.AddOrder(o3);

            c1.create(customers2);
            c2.create(customers2);
            c3.create(customers2);

            foreach (Customer customer in customers2)
            {
                Console.WriteLine(customer.read());
            }

            Console.WriteLine();
            c1.delete(customers2);

            foreach (Customer customer in customers2)
            {
                Console.WriteLine(customer.read());
            }
            Console.ReadKey();

        }
    }

    //clasa cu doi parametrii generici
    class MyClass<T1, T2>
    where T1: class,new()
    where T2: MyInterface
    { 
        static T1 param1 = new T1();
        //static T2 param2 =new T2();
        public void Display()
        {
            Console.WriteLine("Parameter1: "+param1.GetType() + "\nParameter2: "/*+param2.GetType()*/);
        }

        public MyClass()
        {

        }
    }

    interface MyInterface
    {

    }
}
