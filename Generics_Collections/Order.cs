﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics_Collections
{
    public class Order:CRUDInterface<Order>
    {
        public double price { get; set; }
        public string name { get; set; }
        public Order(double price, string name)
        {
            this.price = price;
            this.name = name;
        }

        public override string ToString()
        {
            return "{name:" + name + " price:" + price + "},";
        }

        public void create(List<Order> list)
        {
            if (this != null)
                list.Add(this);
        }

        public string read()
        {
            return ToString();
        }

        public Order update()
        {
            throw new NotImplementedException();
            //update order data
        }

        public void delete(List<Order> list)
        {
            if (this != null)
            {
                list.Remove(this);
            }
        }
    }
}
