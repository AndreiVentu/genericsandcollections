﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics_Collections
{
    public class Customer: CRUDInterface<Customer>
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public List<Order> orders { get; set; }


        public Customer(string Name, int Age)
        {
            this.Name = Name;
            this.Age = Age;
            orders = new List<Order>();
        }

        public Customer()
        {
        }

        public void AddOrder(Order order)
        {
            orders.Add(order);
        }

        public override string ToString()
        {
            return Name + " " + Age;
        }

        public void create(List<Customer> list)
        {
            if(this!=null)
            list.Add(this);
        }

        public string read()
        {
            return ToString();
        }

        public Customer update()
        {
            throw new NotImplementedException();
            //update customer data
        }

        public void delete(List<Customer> list)
        {
            if (this != null)
            {
                list.Remove(this);
            }
        }
    }
}
